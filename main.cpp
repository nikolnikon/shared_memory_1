#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>

#define SIZE 1048576


int main(int argc, char **argv)
{
    key_t key = ftok("/tmp/mem.temp", 1);

    int mem_id = shmget(key, SIZE, IPC_CREAT|0777);
    if (mem_id == -1) {
        perror("shmget");
        return 1;
    }

    char *mem = (char*)shmat(mem_id, NULL, 0);
    if (! mem || ((void*)mem) == (void*)-1) {
        perror("shmat");
        return 1;
    }

    for (int i = 0; i <= SIZE; ++i) {
        char *cell = mem + i;
        *cell = 42;
    }

    shmdt((void*)mem);

    return 0;
}


